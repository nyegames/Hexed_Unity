﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
    public bool lockWin = false;

    public GameScore gameScore;

    public WinAnimationController winAnimation;
    public AddWinToGameScore addGameScore;

    public ChallengeController challengeController;

    public ErrorMessageController errorMessageController;

    private int currentWinSize;
    private string currentWinType;

    // BOMB
    private string bombType = "";

    /// <summary>
    /// When you click a hexagon it will try to form a win to the connecting hexagons
    /// </summary>
    /// <param name="hexCollider"></param>
    public void HexNodeClicked(Collider2D hexCollider, bool cpu = false)
    {
        //if( !lockWin )
        //{
        HexNodeScript node = hexCollider.gameObject.GetComponent<HexNodeScript>();
        if (node == null) return;

        ArrayList win = GetAllNodesOfSameType.getNodes.getAllConnectingNodes(node.tag, node.gridX, node.gridY);

        if (win == null || win.Count < 1 )
        {
            errorMessageController.showErrorMessage(node);
            return;
        }

        currentWinSize = win.Count;
        currentWinType = hexCollider.tag;

        int[][] winArray = new int[win.Count][];

        for( int i =0; i < win.Count; i++)
        {
            HexNodeScript hxNode = ((GameObject)win[i]).GetComponent<HexNodeScript>();
            winArray[i] = new int[] { hxNode.gridX, hxNode.gridY };
        }

        WinAnimationDataObject dataObj = new WinAnimationDataObject();
        dataObj.currentWinCoords = winArray;
        dataObj.currentWinTag = hexCollider.tag;
        dataObj.currentWinAmount = WinCalculations.winCals.calculateWinFromArrayOfNodes(win);

        dataObj.cpu = cpu;

        winAnimation.StartWinAnimation(addGameScore, dataObj);
        //}
    }

    public void StoreBombClicked(string bombType)
    {
        this.bombType = bombType;
    }

    /// <summary>
    /// Clicking a bomb and then clicking a hexagon will destroy a single hexagon and replace it with the bombs colour
    /// </summary>
    /// <param name="hexCollider"></param>
    public void HexNodeBombClicked(Collider2D hexCollider)
    {
        HexNodeScript node = hexCollider.gameObject.GetComponent<HexNodeScript>();
        Destroy(CreateGrid.grid.GAME_GRID[node.gridX, node.gridY]);
        CreateGrid.grid.GAME_GRID[node.gridX,node.gridY] = CreateGrid.grid.genereateObject(node.gridX, node.gridY, this.bombType);
        CreateGrid.grid.GAME_GRID[node.gridX, node.gridY].GetComponent<HexNodeScript>().active = true;

        if (challengeController != null) challengeController.checkForChallenges(gameScore.Score, currentWinSize, currentWinType);
    }

    public void LockWin()
    {
        lockWin = true;
    }

    public void UnlockWin()
    {
        lockWin = false;
    }

    public void WinAnimationFinished()
    {

    }
}
