﻿using UnityEngine;
using System.Collections;

public class WinAnimationDataObject
{
    //Current win information
    public int[][] currentWinCoords = null;
    public string currentWinTag = "";
    public double currentWinAmount = 0;

    public bool cpu = false;

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
