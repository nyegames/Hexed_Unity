﻿using UnityEngine;
using System.Collections;

public class WinCalculations : MonoBehaviour
{
    public static WinCalculations winCals;

    public Hashtable winTable;

    void Awake()
    {
        //Creates a singleton of this class, you can only have 1 active player data class at once.
        if (winCals == null)
        {
            DontDestroyOnLoad(gameObject);
            winCals = this;
        }
        else if (winCals != this)
        {
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// This will check the tag to see how many this symbol has a minimum win, then return that value
    /// </summary>
    /// <param name="symbolTag"></param>
    /// <returns></returns>
    public int getMinSymbolsInWin(string symbolTag)
    {
        return PlayerData.playerData.WinMinimums[getIndexOfHexType(symbolTag)];
    }

    /// <summary>
    /// Give in the list of HexNodes and it will return the win value that those nodes are worth.
    /// </summary>
    /// <param name="win"></param>
    /// <returns></returns>
    public double calculateWinFromArrayOfNodes( ArrayList win )
    {
        if (win == null || win.Count <= 0) return 0;

        //First get the base win for this type of win.
        string hexTag = ((GameObject)win[0]).tag;
        double baseValue = PlayerData.playerData.HexBaseWinValues[getIndexOfHexType(hexTag)];

        double amountInWin = (double)win.Count;

        double totalWin = baseValue * amountInWin;

        totalWin = (int)(totalWin * determineChallengeModifier(hexTag));

        return totalWin;
    }

    /// <summary>
    /// Determines if there is a challenge modifier to use.
    /// Example, score challenges, if not of the correct colour will divide the score by 4
    /// </summary>
    /// <param name="hexTag"></param>
    /// <returns></returns>
    public double determineChallengeModifier(string hexTag)
    {
        //Return a mulitplier of 1 if you have no score challenge
        if (PlayerData.playerData.challengeScoreTarget < 0) return 1.0;
            
        //If the hex type is the favoured type of the challenge return the favoured multiplier :)
        if( hexTag.Equals(PlayerData.playerData.scoreFavouredHexType) )
        {
            return PlayerData.playerData.scoreFavouredTypeIncrease;
        }

        return PlayerData.playerData.scoreWeakTypeDecrease;
    }


    /// <summary>
    /// Will find the index of the hex type, the index is used to access all the information about the type of hex node you want to know about
    /// </summary>
    /// <param name="hexType"></param>
    /// <returns></returns>
    private int getIndexOfHexType(string hexType)
    {
        for( int i =0; i < PlayerData.playerData.HexTypes.Length; i++)
        {
            if (PlayerData.playerData.HexTypes[i].Equals(hexType)) return i;
        }
        return 0;
    }

}
