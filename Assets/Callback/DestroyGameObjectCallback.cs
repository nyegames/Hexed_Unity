﻿using UnityEngine;
using System.Collections;

public class DestroyGameObjectCallback : ICallback
{
    private GameObject gameObject;

    public DestroyGameObjectCallback(GameObject gameObject)
    {
        this.gameObject = gameObject;
    }

    public void Run()
    {
        if (this.gameObject != null) Object.Destroy(this.gameObject);
    }
}
