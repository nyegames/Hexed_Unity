﻿using UnityEngine;
using System.Collections;

public interface ICallback
{
    void Run();
}
