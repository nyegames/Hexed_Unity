﻿using UnityEngine;
using System.Collections;
using System;

public class ChallengeController : MonoBehaviour
{
    public GameLoader loader;
    public CompletedStageController completedStageController;

    void Start()
    {

    }

    public void checkForChallenges(double totalScore, int winSize, string winColour)
    {
        if (checkForScoreChallenge(totalScore)) return;
        if (checkForComboChallenge(winSize, winColour)) return;
        if (checkForRemoveCertainAmountOfHexagons(winSize)) return;
        if (checkForColourDestroyChallenge()) return;
    }

    private bool checkForScoreChallenge(double totalScore)
    {
        string chal = PlayerData.playerData.getCurrentChallengeText();
        if (chal.StartsWith(PlayerData.ScoreText) )
        {
            string challengeScore = chal.Remove(0, PlayerData.ScoreText.Length);
            if( totalScore >= PlayerData.playerData.challengeScoreTarget)
            {
                challengeCompleted();
                return true;
            }
        }

        return false;
    }

    private bool checkForComboChallenge(int winSize, string winColour)
    {
        string chal = PlayerData.playerData.getCurrentChallengeText();
        if (chal.StartsWith(PlayerData.ComboText) )
        {
            if (winSize >= PlayerData.playerData.challengeWinChain && winColour.Equals(PlayerData.playerData.challengeColourType) )
            {
                challengeCompleted();
                return true;
            }
        }
        return false;
    }

    private bool checkForColourDestroyChallenge()
    {
        string chal = PlayerData.playerData.getCurrentChallengeText();
        if (chal.StartsWith(PlayerData.DestroyText) )
        {
            if( !CreateGrid.grid.doesGridContainer(PlayerData.playerData.challengeColourType))
            {
                challengeCompleted();
                return true;
            }
        }
        return false;
    }

    private bool checkForRemoveCertainAmountOfHexagons(int winSize)
    {
        PlayerData.playerData.challengeHexagonsDestroyed -= winSize;

        string chal = PlayerData.playerData.getCurrentChallengeText();
        if (chal.StartsWith(PlayerData.RemoveText))
        {
            if( PlayerData.playerData.challengeHexagonsDestroyed <= 0 )
            {
                challengeCompleted();
                return true;
            }
        }
        return false;

    }


    private void challengeCompleted()
    {
        completedStageController.challengeComplete(loader);
    }

}
