﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PaintBrushController : MonoBehaviour
{
    public MsgMouseClicked mouseClicked;

    public GameObject[] bombObjects;
    public Sprite[] allBombImages;
    public GameObject bombPanel;

    private Image[] bombImages;
    private SingleBomb[] bombs;

    [SerializeField]
    private double bombPoints = 0;//Max is bombObjects.Length * 100;
    //Normally 500, each 100 means 1 more active bomb
    private double singleBombValue = 100;

    private double bombPointsPerScore = 1.0;//For each 1 score, get this in bomb points

	// Use this for initialization
	void Start ()
    {
        bombImages = new Image[bombObjects.Length];
        bombs = new SingleBomb[bombObjects.Length];
        for (int i = 0; i < bombObjects.Length; i++)
        {
            bombImages[i] = bombObjects[i].GetComponent<Image>();
            bombs[i] = null;
            bombObjects[i].SetActive(false);
        }
    }
    
    private SingleBomb createBombObject(int index)
    {
        Vector2 loc = new Vector2(bombImages[index].rectTransform.localPosition.x, bombImages[index].rectTransform.localPosition.y);
        int typeIndex = UnityEngine.Random.Range(0, PlayerData.playerData.HexTypes.Length);
        SingleBomb bomb = new SingleBomb(loc, PlayerData.playerData.HexTypes[typeIndex],typeIndex);

        bombImages[index].sprite = allBombImages[typeIndex];

        return bomb;
    }

    private void removeBomb(int index)
    {
        bombs[index] = null;
        bombObjects[index].SetActive(false);
        bombPoints -= singleBombValue;
        if (bombPoints < 0.0) bombPoints = 0.0;
    }

    /// <summary>
    /// You get given the score per hex, need to calculate how much each point is worth in bomb cooldown rate. Then reduce the cooldown for the bombs
    /// </summary>
    /// <param name="winPerHex"></param>
    public void updateBombPoints(double winPerHex)
    {
        addBombPoints((double)this.bombPointsPerScore * (double)winPerHex);
        checkForBombPointsToSpawnBombs();
    }

    private void addBombPoints(double addBombPoints)
    {
        if(bombPoints > bombObjects.Length * singleBombValue)
        {
            bombPoints = bombObjects.Length * singleBombValue;
            return;
        }
        this.bombPoints += addBombPoints;
    }

    /// <summary>
    /// Have you got enough points to make a bomb visible?
    /// </summary>
    private void checkForBombPointsToSpawnBombs()
    {
        if (bombPoints < singleBombValue) return;

        int activeBombs = getActiveBombs();

        for( int i =0; i < activeBombs; i++)
        {
            if (i >= bombObjects.Length) return;

            if( bombs[i] == null) createBombAtLocation(i);
        }
    }

    private int getActiveBombs()
    {
        return (int)bombPoints / (int)singleBombValue;
    }

    /// <summary>
    /// Creates a new bomb for you to use
    /// </summary>
    /// <param name="index"></param>
    private void createBombAtLocation(int index)
    {
        bombs[index] = createBombObject(index);
        bombObjects[index].SetActive(true);
    }

    public void bombClicked(int index)
    {
        mouseClicked.setBombInformation(bombs[index].type);

        removeBomb(index);
        //If you clicked the top bomb, don't care, no need to reshuffle.
        if (index == getActiveBombs()) return;

        //Loop through all the bombs, starting at the place you clicked and going to the top
        for( int i =index; i < getActiveBombs(); i++)
        {
            bombObjects[i].SetActive(true);
            bombObjects[i+1].SetActive(false);
            //For every bomb move it down 1
            bombs[i] = bombs[i + 1];
            bombObjects[i].GetComponent<Image>().sprite = allBombImages[bombs[i].index];
        }
        bombObjects[getActiveBombs()].SetActive(false);
        bombs[getActiveBombs()] = null;

    }

    // Update is called once per frame
    void Update ()
    {

	}
}

public class SingleBomb
{
    public SingleBomb(Vector2 loc, string type, int index)
    {
        this.location = loc;
        this.type = type;
        this.index = index;
    }

    public Vector2 location;
    public string type;
    public int index;

}
