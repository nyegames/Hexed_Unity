﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowChallengeInformation : MonoBehaviour
{
    public Text challengeInfo;

    private float currentTime = 0.0f;
    private float internval = 0.5f;

    void Start()
    {
        showChallengeInformation();
    }

    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > internval)
        {
            currentTime = 0.0f;
            showChallengeInformation();
        }
    }

    public void showChallengeInformation()
    {
        if (challengeInfo!=null)challengeInfo.text = PlayerData.playerData.getCurrentChallengeText(true);
    }
}
