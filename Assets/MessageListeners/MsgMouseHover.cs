﻿using UnityEngine;
using System.Collections;

public class MsgMouseHover : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        RaycastHit2D hit = objectAtMousePosition();
        if (hit.collider != null)
        {
            SendMessageUpwards("HexNodeHover", hit.collider);
        }
    }

    private RaycastHit2D objectAtMousePosition()
    {
        return Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
    }
}
